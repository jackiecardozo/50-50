# 50+50

50 +50 is a women-led group that aims to encourage the professional development of women in the digital and tech industries.

## Getting Started

This project was build using Fractal environment. Please, check Fractal's requirements [here](https://fractal.build/guide/#requirements).

### Installing

Download or clone the repo. Run the command on you terminal into the project folder:

```
npm install
```

Please, run this command everytime a asset is added to the project (images, css and js files):

```
gulp build
```

To run the environment:

```
gulp develop
```

At your browser, go to:

```
http://localhost:3000
```

You can check the components individually, but for this project the most important files are at /templates.

At your Editor, check that all this templates have .config.yml files. This is the data layer for the websites. 
You can update it as necessary and then build the project to generate the html files.

## Deployment

To deploy, run the command:

```
fractal build
```

A /dest folder will be generated. At dest/components/preview, pick the files necessary (index.html, how-it-works.html, team.html) 
and paste at dest. 

You'll need to manually change all assets urls at these 3 files, like

```
../../css/entry.css
```

to

```
css/entry.css
```

for css, js and images.

Open html files and check if everything is working properly.

Now, you can upload /css, /scripts, /images, index.html, how-it-works.html, team.html to the hosting system.

## Built With

* [Fractal](https://fractal.build/) - Fractal Build


## Authors

* **Jackie Cardozo** - *Initial work* - [jackiecard](http://github.com/jackiecard)

## Nice Reads

* [Handlebars](https://handlebarsjs.com/) - Everytime you see a {{something}} or {{#if something}} or {{each something}} that's the templater, handlebars.
* [BEM](http://getbem.com/introduction/) - Nice way to build mantainable CSS code.
* [YML](https://en.wikipedia.org/wiki/YAML) - Way to write data/content.