"use strict";
/*
 * Require the Fractal module
 */
const fractal = (module.exports = require("@frctl/fractal").create());

fractal.set('project.title', '50+50 Women in Digital'); // title for the project
fractal.web.set('builder.dest', 'dest/'); // destination for the static export
fractal.docs.set('path', `${__dirname}/docs`); // location of the documentation directory.
fractal.components.set('path', `${__dirname}/src/components`); // location of the component directory.
fractal.components.set('default.preview', '@preview');
fractal.web.set("static.path", `${__dirname}/public`);

/**
 * Customize handlebars
 */
const hbs = require("@frctl/handlebars")({
  helpers: {
    ifCond: (v1, operator, v2, options) => {
      switch (operator) {
        case "==":
          return v1 == v2 ? options.fn(this) : options.inverse(this);
        case "===":
          return v1 === v2 ? options.fn(this) : options.inverse(this);
        case "!=":
          return v1 != v2 ? options.fn(this) : options.inverse(this);
        case "!==":
          return v1 !== v2 ? options.fn(this) : options.inverse(this);
        case "<":
          return v1 < v2 ? options.fn(this) : options.inverse(this);
        case "<=":
          return v1 <= v2 ? options.fn(this) : options.inverse(this);
        case ">":
          return v1 > v2 ? options.fn(this) : options.inverse(this);
        case ">=":
          return v1 >= v2 ? options.fn(this) : options.inverse(this);
        case "&&":
          return v1 && v2 ? options.fn(this) : options.inverse(this);
        case "||":
          return v1 || v2 ? options.fn(this) : options.inverse(this);
        default:
          return options.inverse(this);
      }
    }
  }
});

fractal.components.engine(hbs);

/* change the "assets" tab to the present file type */
fractal.components.set("resources", {
  scss: {
    label: "SCSS",
    match: ["**/*.scss"]
  },
  css: {
    label: "CSS",
    match: ["**/*.css"]
  },
  other: {
    label: "Other Assets",
    match: ["**/*", "!**/*.scss", "!**.css"]
  }
});

