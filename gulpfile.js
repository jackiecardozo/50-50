'use strict';

const gulp = require('gulp');
const sass = require("gulp-sass");
const rename = require("gulp-rename");
const sassGlob = require("gulp-sass-glob");
const uglify = require("gulp-uglify");
const fractal = require("./fractal.js");
const logger = fractal.cli.console;
const cleanCSS = require("gulp-clean-css");

gulp.task('fractal', function(){
    const server = fractal.web.server({
        sync: true
    });
    server.on('error', err => logger.error(err.message));
    return server.start().then(() => {
        logger.success(`Fractal server is now running at ${server.url}`);
    });
});

gulp.task('fractal:build', function(){
    const builder = fractal.web.builder();
    builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
    builder.on('error', err => logger.error(err.message));
    return builder.build().then(() => {
        logger.success('Fractal build completed!');
    });
});

gulp.task("styles", function(done) {
  gulp
    .src(["src/styles/entry.scss"])
    .pipe(sassGlob())
    .pipe(sass().on("error", sass.logError))
    .pipe(rename("entry.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest("./public/css"));
    done();
});

gulp.task('scripts', function() {
  return gulp.src('./src/scripts/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./public/scripts'))
});

gulp.task("images", function() {
  return gulp
    .src(["src/images/**"], {
      base: "src/"
    })
    .pipe(gulp.dest("./public/"));
});

gulp.task('watch', function(){
  gulp.watch("src/**/*.js", gulp.series("scripts")); 
  gulp.watch("src/**/*.scss", gulp.series("styles")); 
  gulp.watch("src/images/**", gulp.series("images")); 
})


gulp.task("build", gulp.parallel("scripts", "styles", "images", "fractal"));
gulp.task("develop", gulp.parallel("watch", "fractal"));