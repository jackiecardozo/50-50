var burger = document.querySelector(".burger");
var menu = document.querySelector(".menu");
var body = document.querySelector("body");
burger.addEventListener("click", function(e) {
  e.target.classList.toggle("burger--close");
  if (menu) {
    menu.classList.toggle("menu--show");
    body.classList.toggle("disable-scroll");
  }
});
